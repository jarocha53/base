<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class BasesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {}


/**
 * charts method
 *
 * @return void
 */
	public function charts(){}


/**
 * tables method
 *
 * @return void
 */
	public function tables(){}


/**
 * forms method
 *
 * @return void
 */
	public function forms(){}


/**
 * elements method
 *
 * @return void
 */
	public function elements(){}


/**
 * grid method
 *
 * @return void
 */
	public function grid(){}


/**
 * blank method
 *
 * @return void
 */
	public function blank(){}

}