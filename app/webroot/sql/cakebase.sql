/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50612
Source Host           : localhost:3306
Source Database       : cakebase

Target Server Type    : MYSQL
Target Server Version : 50612
File Encoding         : 65001

Date: 2014-09-12 08:39:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cake_sessions
-- ----------------------------
DROP TABLE IF EXISTS `cake_sessions`;
CREATE TABLE `cake_sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `data` text COLLATE utf8_unicode_ci,
  `expires` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of cake_sessions
-- ----------------------------
INSERT INTO `cake_sessions` VALUES ('1i6hr34cc66fb4762i4bfsj9t1', 'Config|a:3:{s:9:\"userAgent\";s:32:\"664a4e3faf665e4820fc8d4b15881dc1\";s:4:\"time\";i:1410381947;s:9:\"countdown\";i:10;}Message|a:0:{}Auth|a:1:{s:4:\"User\";a:14:{s:2:\"id\";s:1:\"1\";s:8:\"group_id\";s:1:\"1\";s:8:\"username\";s:5:\"admin\";s:5:\"email\";s:15:\"admin@admin.com\";s:6:\"nombre\";s:5:\"admin\";s:14:\"primerApellido\";s:5:\"admin\";s:15:\"segundoApellido\";s:5:\"admin\";s:9:\"telefonos\";s:0:\"\";s:9:\"direccion\";s:0:\"\";s:7:\"created\";s:19:\"2014-06-03 21:07:02\";s:8:\"modified\";s:19:\"2014-09-09 21:08:59\";s:14:\"changePassword\";b:0;s:6:\"active\";b:0;s:5:\"Group\";a:4:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:13:\"administrator\";s:7:\"created\";s:19:\"2014-06-03 21:06:28\";s:8:\"modified\";s:19:\"2014-06-03 21:06:28\";}}}', '1410381948');
INSERT INTO `cake_sessions` VALUES ('52fd40euvhgijdcidn3mc9v6f7', 'Config|a:3:{s:9:\"userAgent\";s:32:\"6106c9a8527bcc8434a282e84227c519\";s:4:\"time\";i:1410381425;s:9:\"countdown\";i:10;}Message|a:0:{}Auth|a:1:{s:8:\"redirect\";s:1:\"/\";}', '1410381425');
INSERT INTO `cake_sessions` VALUES ('go3bhdoo6bhobp53kj9ilc1us0', 'Config|a:3:{s:9:\"userAgent\";s:32:\"664a4e3faf665e4820fc8d4b15881dc1\";s:4:\"time\";i:1410396388;s:9:\"countdown\";i:10;}Message|a:0:{}Auth|a:1:{s:4:\"User\";a:14:{s:2:\"id\";s:1:\"1\";s:8:\"group_id\";s:1:\"1\";s:8:\"username\";s:5:\"admin\";s:5:\"email\";s:15:\"admin@admin.com\";s:6:\"nombre\";s:5:\"admin\";s:14:\"primerApellido\";s:5:\"admin\";s:15:\"segundoApellido\";s:5:\"admin\";s:9:\"telefonos\";s:0:\"\";s:9:\"direccion\";s:0:\"\";s:7:\"created\";s:19:\"2014-06-03 21:07:02\";s:8:\"modified\";s:19:\"2014-09-09 21:08:59\";s:14:\"changePassword\";b:0;s:6:\"active\";b:0;s:5:\"Group\";a:4:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:13:\"administrator\";s:7:\"created\";s:19:\"2014-06-03 21:06:28\";s:8:\"modified\";s:19:\"2014-06-03 21:06:28\";}}}', '1410396388');
INSERT INTO `cake_sessions` VALUES ('kb480oof30flukmb7roela1eb5', 'Config|a:3:{s:9:\"userAgent\";s:32:\"664a4e3faf665e4820fc8d4b15881dc1\";s:4:\"time\";i:1410398685;s:9:\"countdown\";i:10;}Message|a:0:{}Auth|a:1:{s:4:\"User\";a:14:{s:2:\"id\";s:1:\"1\";s:8:\"group_id\";s:1:\"1\";s:8:\"username\";s:5:\"admin\";s:5:\"email\";s:15:\"admin@admin.com\";s:6:\"nombre\";s:5:\"admin\";s:14:\"primerApellido\";s:5:\"admin\";s:15:\"segundoApellido\";s:5:\"admin\";s:9:\"telefonos\";s:0:\"\";s:9:\"direccion\";s:0:\"\";s:7:\"created\";s:19:\"2014-06-03 21:07:02\";s:8:\"modified\";s:19:\"2014-09-09 21:08:59\";s:14:\"changePassword\";b:0;s:6:\"active\";b:0;s:5:\"Group\";a:4:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:13:\"administrator\";s:7:\"created\";s:19:\"2014-06-03 21:06:28\";s:8:\"modified\";s:19:\"2014-06-03 21:06:28\";}}}', '1410398685');

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES ('1', 'administrator', '2014-06-03 21:06:28', '2014-06-03 21:06:28');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `primerApellido` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `segundoApellido` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefonos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `changePassword` tinyint(1) DEFAULT '0',
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '1', 'admin', '3f980491c14aec3ce63ada240d55b0ac740b95c7', 'admin@admin.com', 'admin', 'admin', 'admin', '', '', '2014-06-03 21:07:02', '2014-09-09 21:08:59', '0', '0');
