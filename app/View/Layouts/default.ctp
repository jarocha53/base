<!DOCTYPE html>
<html lang="en">

<head>

    <?php echo $this->Html->charset(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title_for_layout; ?></title>
    <?php
		echo $this->Html->meta('icon');

		//echo $this->Html->css('cake.generic');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>

    <!-- Bootstrap Core CSS -->
    <?php echo $this->Html->css('bootstrap.min');?>

    <!-- Custom CSS -->
     <?php echo $this->Html->css('sb-admin');?>

    <!-- Morris Charts CSS -->
    <?php echo $this->Html->css('plugins/morris');?>
    

    <!-- Custom Fonts -->
     <?php echo $this->Html->css('awesome/font-awesome.min');?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php echo $this->Html->link(
                                   "Sistema Base",
                                    array('controller'=>'bases','action'=>'index'),array('class'=>'navbar-brand','escape'=>false ));  
                        ?>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-footer">
                            <a href="#">Read All New Messages</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Smith <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <?php echo $this->Html->link(
                                    $this->Html->tag('i', '', 
                                            array('class' => 'fa fa-fw fa-power-off'))." Log Out",
                                    array('controller'=>'Users','action'=>'logout'),array('escape'=>false ));  
                        ?>

                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <?php echo $this->Html->link(
                                    $this->Html->tag('i', '', 
                                            array('class' => 'fa fa-fw fa-dashboard'))." Dashboard",
                                    array('controller'=>'bases','action'=>'index'),array('escape'=>false ));  
                        ?>
                    </li>
                    <li>
                          <?php echo $this->Html->link(
                                    $this->Html->tag('i', '', 
                                            array('class' => 'fa fa-fw fa-bar-chart-o'))." Charts",
                                    array('controller'=>'bases','action'=>'charts'),array('escape'=>false ));  
                        ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link(
                                    $this->Html->tag('i', '', 
                                            array('class' => 'fa fa-fw fa-table'))." tables",
                                    array('controller'=>'bases','action'=>'Tables'),array('escape'=>false ));  
                        ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link(
                                    $this->Html->tag('i', '', 
                                            array('class' => 'fa fa-fw fa-edit'))." Forms",
                                    array('controller'=>'bases','action'=>'forms'),array('escape'=>false ));  
                        ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link(
                                    $this->Html->tag('i', '', 
                                            array('class' => 'fa fa-fw fa-desktop'))." Bootstrap Elements",
                                    array('controller'=>'bases','action'=>'elements'),array('escape'=>false ));  
                        ?>
                    </li>
                    <li>
                         <?php echo $this->Html->link(
                                    $this->Html->tag('i', '', 
                                            array('class' => 'fa fa-fw fa-wrench'))." Bootstrap Grid",
                                    array('controller'=>'bases','action'=>'grid'),array('escape'=>false ));  
                        ?>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <?php echo $this->Html->link(
                                    $this->Html->tag('i', '', 
                                            array('class' => 'fa fa-fw fa-file'))." Blank Page",
                                    array('controller'=>'bases','action'=>'blank'),array('escape'=>false ));  
                        ?>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">
                <?php echo $this->Session->flash(); ?>
				<?php echo $this->fetch('content'); ?>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery Version 1.11.0 -->
    <?php echo $this->Html->script("jquery-1.11.0")?>

    <!-- Bootstrap Core JavaScript -->
    <?php echo $this->Html->script("bootstrap.min")?>
    

<!-- Morris Charts JavaScript -->
    <?php echo $this->Html->script("plugins/morris/raphael.min")?>
    <?php echo $this->Html->script("plugins/morris/morris.min")?>
    <?php echo $this->Html->script("plugins/morris/morris-data")?>

<!-- Flot Charts JavaScript -->
    <!--[if lte IE 8]><?php echo $this->Html->script("excanvas.min")?><![endif]-->
    <?php echo $this->Html->script("plugins/flot/jquery.flot")?>
    <?php echo $this->Html->script("plugins/flot/jquery.flot.tooltip.min")?>
    <?php echo $this->Html->script("plugins/flot/jquery.flot.resize")?>
    <?php echo $this->Html->script("plugins/flot/jquery.flot.pie")?>
    <?php echo $this->Html->script("plugins/flot/flot-data")?>
<footer style="background: grey">
		<?php echo $this->element('sql_dump'); ?>
</footer>>

</body>

</html>
